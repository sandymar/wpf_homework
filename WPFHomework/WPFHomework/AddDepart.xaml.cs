﻿using System.Data;
using System.Windows;
using System.Windows.Controls;

namespace WPFHomework
{
    /// <summary>
    /// Логика взаимодействия для AddDepart.xaml
    /// </summary>
    public partial class AddDepart : Window, IView
    {
        private DataRow ResultRow;

        /// <summary>
        /// Конструктор формы
        /// </summary>
        /// <param name="dataRow">Строка с данными</param>
        public AddDepart(DataRow dataRow)
        {
            InitializeComponent();

            ResultRow = dataRow;

            btnSave.Click += BtnSave_Click;
            btnBack.Click += BtnBack_Click;
        }

        public DataTable EmpData { get; set; }
        public DataTable DepData { get; set; }

        public DataGrid DGEmpl { get; }
        public ListView LVDep { get; }

        /// <summary>
        /// Вызывается при нажатии по кнопке "Отмена"
        /// </summary>
        private void BtnBack_Click(object sender, RoutedEventArgs e) => DialogResult = false;

        /// <summary>
        /// Вызывается при нажатии по кнопке "Сохранить"
        /// </summary>
        private void BtnSave_Click(object sender, RoutedEventArgs e)
        {
            ResultRow["Description"] = tbDepartName.Text.Trim();
            DialogResult = true;
        }

        /// <summary>
        /// Вызывается после загрузки формы
        /// </summary>
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            tbDepartName.Text = ResultRow["Description"].ToString().Trim();
        }
    }
}
