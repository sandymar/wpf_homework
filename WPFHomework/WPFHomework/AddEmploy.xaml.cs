﻿using System.Data;
using System.Windows;
using System.Windows.Controls;

namespace WPFHomework
{
    /// <summary>
    /// Логика взаимодействия для AddEmploy.xaml
    /// </summary>
    public partial class AddEmploy : Window
    {
        private DataRow DataRow;
        /// <summary>
        /// Конструктор формы
        /// </summary>
        /// <param name="dataRow">Строка с данными</param>
        public AddEmploy(DataRow dataRow)
        {
            InitializeComponent();

            DataRow = dataRow;
            btnSave.Click += BtnSave_Click;
            btnBack.Click += BtnBack_Click;
        }

        /// <summary>
        /// обработчик нажания по кнопке "Отмена"
        /// </summary>
        private void BtnBack_Click(object sender, RoutedEventArgs e) => DialogResult = false;

        /// <summary>
        /// обработчик нажания по кнопке "Сохранить"
        /// </summary>
        private void BtnSave_Click(object sender, RoutedEventArgs e)
        {
            DataRow["FirstName"]    = tbEmpName.Text.Trim();
            DataRow["LastName"]     = tbEmpLastName.Text.Trim();
            DataRow["Age"]          = tbEmpAge.Text.ReturnInt();
            DataRow["Salary"]       = tbEmpSalary.Text.ReturnInt();
            if (cbEmpDepartmen.SelectedItem != null)
            {
                DataRow["Department"] = ((DataRowView)cbEmpDepartmen.SelectedItem)?.Row["id"];
            }
            DialogResult = true;
        }

        public DataTable EmpData { get; set; }
        public DataTable DepData { get; set; }

        /// <summary>
        /// Вызывается после загрузки формы
        /// </summary>
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            tbEmpName.Text      = DataRow["FirstName"].ToString().Trim();
            tbEmpLastName.Text  = DataRow["LastName"].ToString().Trim();
            tbEmpAge.Text       = DataRow["Age"].ToString().Trim();
            tbEmpSalary.Text    = DataRow["Salary"].ToString().Trim();
        }

        public ComboBox CBDep { get => cbEmpDepartmen; }
    }
}
