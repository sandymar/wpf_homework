﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace WPFHomework
{
    /// <summary>
    /// Класс предназначен для работы с данными из внешнего источника
    /// </summary>
    class Data:IModel
    {
        private List<Employe> emplList;
        private List<Department> departList;
        private SqlDataAdapter empAdapter;
        private SqlDataAdapter depAdapter;
        private SqlConnection connection;

        /// <summary>
        /// Конструктор класса по умолчанию. Подключается к БД и инициализирует адаптеры
        /// </summary>
        public Data()
        {
            ConnectToDB();
            InitDepAdapter();
            InitEmpAdapter();
        }

        public List<Employe> EmplList { get => emplList; }
        public List<Department> DepartList { get => departList; }
        public SqlDataAdapter EmpAdapter { get => empAdapter;}
        public SqlDataAdapter DepAdapter { get => depAdapter;}

        /// <summary>
        /// Инициализирует адаптер для работы с таблицей отделов
        /// </summary>
        private void InitDepAdapter()
        {
            //отделы
            string selectDep = "SELECT * FROM Departments";
            SqlCommand selectDepCommand = new SqlCommand(selectDep, connection);
            depAdapter = new SqlDataAdapter();

            string insertDep = @"INSERT INTO Departments (Description) " +
                "VALUES (@Description); SET @ID = @@IDENTITY";
            SqlCommand insertDepCommand = new SqlCommand(insertDep, connection);
            insertDepCommand.Parameters.Add("@Description", SqlDbType.NVarChar, -1, "Description");
            insertDepCommand.Parameters.Add("@ID", SqlDbType.Int, -1, "ID").Direction = ParameterDirection.Output;

            string updateDep = @"UPDATE Departments SET Description = @Description WHERE ID = @ID";
            SqlCommand updateDepCommand = new SqlCommand(updateDep, connection);
            updateDepCommand.Parameters.Add("@Description", SqlDbType.NVarChar, -1, "Description");
            updateDepCommand.Parameters.Add("@ID", SqlDbType.Int, -1, "ID");

            string deleteDep = @"DELETE FROM Departments WHERE ID=@ID";
            SqlCommand deleteDepCommand = new SqlCommand(deleteDep, connection);
            deleteDepCommand.Parameters.Add("@ID", SqlDbType.Int, -1, "ID");

            DepAdapter.SelectCommand = selectDepCommand;
            DepAdapter.InsertCommand = insertDepCommand;
            DepAdapter.UpdateCommand = updateDepCommand;
            DepAdapter.DeleteCommand = deleteDepCommand;
        }

        /// <summary>
        /// Инициализирует адаптер для работы с таблицей сотрудников
        /// </summary>
        private void InitEmpAdapter()
        {
            //сотрудники
            string selectEmp = "SELECT * FROM Employes WHERE Department = @Department";
            empAdapter = new SqlDataAdapter();
            SqlCommand selectCommand = new SqlCommand(selectEmp, connection);
            selectCommand.Parameters.Add("@Department", SqlDbType.Int, -1);
            selectCommand.Parameters["@Department"].Value = 0;

            string insertEmp = @"INSERT INTO Employes (FirstName,LastName,Age,Salary,Department) " +
                "VALUES (@FirstName,@LastName,@Age,@Salary,@Department); SET @ID = @@IDENTITY";
            SqlCommand insertCommand = new SqlCommand(insertEmp, connection);
            insertCommand.Parameters.Add("@FirstName", SqlDbType.NVarChar, 100, "FirstName");
            insertCommand.Parameters.Add("@LastName", SqlDbType.NVarChar, 100, "LastName");
            insertCommand.Parameters.Add("@Age", SqlDbType.Int, -1, "Age");
            insertCommand.Parameters.Add("@Salary", SqlDbType.Int, -1, "Salary");
            insertCommand.Parameters.Add("@Department", SqlDbType.Int, -1, "Department");
            insertCommand.Parameters.Add("@ID", SqlDbType.Int, -1, "ID").Direction = ParameterDirection.Output;

            string updateEmp = @"UPDATE Employes SET FirstName = @FirstName, LastName = @LastName, Age = @Age," +
                 "Salary = @Salary, Department = @Department WHERE ID = @ID";
            SqlCommand updateCommand = new SqlCommand(updateEmp, connection);
            updateCommand.Parameters.Add("@FirstName", SqlDbType.NVarChar, 100, "FirstName");
            updateCommand.Parameters.Add("@LastName", SqlDbType.NVarChar, 100, "LastName");
            updateCommand.Parameters.Add("@Age", SqlDbType.Int, -1, "Age");
            updateCommand.Parameters.Add("@Salary", SqlDbType.Int, -1, "Salary");
            updateCommand.Parameters.Add("@Department", SqlDbType.Int, -1, "Department");
            updateCommand.Parameters.Add("@ID", SqlDbType.Int, -1, "ID");

            string deleteEmp = @"DELETE FROM Employes WHERE ID=@ID";
            SqlCommand deleteCommand = new SqlCommand(deleteEmp, connection);
            deleteCommand.Parameters.Add("@ID", SqlDbType.Int, -1, "ID");

            empAdapter.SelectCommand = selectCommand;
            empAdapter.InsertCommand = insertCommand;
            empAdapter.UpdateCommand = updateCommand;
            empAdapter.DeleteCommand = deleteCommand;
        }

        /// <summary>
        /// Инициализирует подключение к БД
        /// </summary>
        private void ConnectToDB()
        {
            var connectionString = ConfigurationManager.ConnectionStrings["DataConnectionString"].ConnectionString;
            connection = new SqlConnection(connectionString);
            connection.Open();
        }

    }
}
