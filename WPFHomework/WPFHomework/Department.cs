﻿namespace WPFHomework
{
    /// <summary>
    /// Подразделения
    /// </summary>
    public class Department
    {
        private int depId;
        private string depName;

        /// <summary>
        /// Конструктор нового подразделения
        /// </summary>
        /// <param name="id">Идентификатор подразделения</param>
        /// <param name="name">Название подразделения</param>
        public Department(int id, string name)
        {
            DepId = id;
            DepName = name;
        }

        /// <summary>
        /// Возвращает строковое представление подразделения
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return DepName;
        }

        public int DepId { get => depId; set => depId = value; }
        public string DepName { get => depName; set => depName = value; }

    }
}