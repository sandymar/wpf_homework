﻿namespace WPFHomework
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    public class Employe
    {
        private string firstName;
        private string lastName;
        private int age;
        private int salary;
        private int departIndex;

        /// <summary>
        /// Конструктор сотрудника с пустыми данными
        /// </summary>
        public Employe() : this("", "", 0, 0, 0) { }

        /// <summary>
        /// Конструктор нового сотрудника
        /// </summary>
        /// <param name="firstName">Имя</param>
        /// <param name="lastName">Фамилия</param>
        /// <param name="age">Возраст</param>
        /// <param name="salary">Зарплата</param>
        /// <param name="departIndex">Идентификатор отдела</param>
        public Employe(string firstName, string lastName, int age, int salary, int departIndex)
        {
            FirstName = firstName;
            LastName = lastName;
            Age = age;
            Salary = salary;
            DepartIndex = departIndex;
        }

        /// <summary>
        /// Возвращает строковое представление сотрудника
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return $"{FirstName} {LastName}";
        }

        public string FirstName { get => firstName; set => firstName = value; }
        public string LastName { get => lastName; set => lastName = value; }
        public int Age { get => age; set => age = value; }
        public int Salary { get => salary; set => salary = value; }
        public int DepartIndex { get => departIndex; set => departIndex = value; }
    }
}