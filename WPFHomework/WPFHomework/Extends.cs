﻿using System;

namespace WPFHomework
{
    /// <summary>
    /// Класс для определения расширений
    /// </summary>
    static class Extends
    {
        /// <summary>
        /// Расширение проверяет, состоит ли строка из числовых символов
        /// </summary>
        /// <param name="s">Проверяемая строка</param>
        /// <returns>Возвращает, все ли символы строки являются числовыми</returns>
        public static bool IsDigit(this string s)
        {
            foreach (char c in s) if (!Char.IsDigit(c)) return false;
            return true;
        }

        /// <summary>
        /// Расшрение возвращает целое число из строки, если конвертация возможна. Если невозможна - возвращает ноль
        /// </summary>
        /// <param name="s">Строка для конвертации</param>
        /// <returns></returns>
        public static int ReturnInt(this string s)
        {
            if (String.IsNullOrEmpty(s)) s = "0";
            return s.IsDigit() ? Convert.ToInt32(s) : 0;
        }
    }
}
