﻿using System.Data.SqlClient;

namespace WPFHomework
{
    /// <summary>
    /// Интерфейс, описывающий обязательные свойства текущей модели.
    /// </summary>
    interface IModel
    {
        SqlDataAdapter EmpAdapter { get; }
        SqlDataAdapter DepAdapter { get; }

    }
}
