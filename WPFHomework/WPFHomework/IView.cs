﻿using System.Data;
using System.Windows.Controls;

namespace WPFHomework
{
    /// <summary>
    /// Интерфейс, описывающий необходимый состав свойств основной формы
    /// </summary>
    public interface IView
    {
        DataTable EmpData { get; set; }
        DataTable DepData { get; set; }
        DataGrid DGEmpl { get; }
        ListView LVDep { get; }
    }
}
