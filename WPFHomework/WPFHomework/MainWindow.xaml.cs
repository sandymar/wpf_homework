﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Windows;
using System.Windows.Controls;

namespace WPFHomework
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, IView
    {
        Presenter presenter;

        private DataTable empData;
        private DataTable depData;

        /// <summary>
        /// Точка входа в приложение
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();

            presenter = new Presenter(this);

            btnAddEmp.Click += (s,e)  => presenter.AddEmploye();
            btnDelEmp.Click += (s, e) => presenter.DeleteEmploye();
            btnAddDepart.Click += (s,e) => presenter.AddDepart();
            btnDelDepart.Click += (s,e) => presenter.DeleteDepart();

            ListViewDepartment.SelectionChanged += (s,e) => presenter.UpdateDeparts();
            ListViewDepartment.MouseDoubleClick += (s,e) => presenter.ChangeDepartment();
            DataGridEmployers.MouseDoubleClick += (s, e) => presenter.ChangeEmploy();
        }
   
        public DataTable EmpData { get => empData; set => empData = value; }
        public DataTable DepData { get => depData; set => depData = value; }
        public DataGrid DGEmpl { get => DataGridEmployers;}
        public ListView LVDep { get => ListViewDepartment;}
    }
}
