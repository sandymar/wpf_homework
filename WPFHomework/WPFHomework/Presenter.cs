﻿using System.Data;

namespace WPFHomework
{
    /// <summary>
    /// Класс создан в рамках практики по применению MVP-паттерна
    /// Является презентором для двуъ WPF-форм.
    /// </summary>
    public class Presenter
    {
        private IView view;     ///View
        private IModel data;          ///Model

        /// <summary>
        /// Констрактор по умолчанию
        /// </summary>
        /// <param name="mainWindow">Форма с основными данными</param>
        public Presenter(IView mainWindow)
        {
            view = mainWindow;
            LoadData();
        }

        /// <summary>
        /// Загрузка первоначальных данных
        /// </summary>
        private void LoadData()
        {
            data = new Data();

            view.DepData = new DataTable();
            data.DepAdapter.Fill(view.DepData);
            view.LVDep.DataContext = view.DepData.DefaultView;

            view.EmpData = new DataTable();
            data.EmpAdapter.Fill(view.EmpData);
            view.DGEmpl.DataContext = view.EmpData.DefaultView;
        }

        #region Обработчики событий основной формы
        /// <summary>
        /// Вызывается при двойном клике по элементу списка отделов
        /// </summary>
        public void ChangeDepartment()
        {
            DataRowView tempDepart = (DataRowView)view.LVDep.SelectedItem;
            tempDepart.BeginEdit();
            AddDepart addDepart = new AddDepart(tempDepart.Row);
            addDepart.ShowDialog();
            if (addDepart.DialogResult.HasValue && addDepart.DialogResult.Value)
            {
                tempDepart.EndEdit();
                data.DepAdapter.Update(view.DepData);
            }
        }

        /// <summary>
        /// Вызывается при двойном клике по элементу списка сотрудников
        /// </summary>
        public void ChangeEmploy()
        {
            DataRowView tempDepart = (DataRowView)view.DGEmpl.SelectedItem;
            tempDepart.BeginEdit();
            AddEmploy addEmploy = new AddEmploy(tempDepart.Row);

            addEmploy.CBDep.ItemsSource = view.DepData.DefaultView;
            addEmploy.CBDep.SelectedItem = view.LVDep.SelectedItem;
            
            addEmploy.ShowDialog();
            if (addEmploy.DialogResult.HasValue && addEmploy.DialogResult.Value)
            {
                tempDepart.EndEdit();
                data.EmpAdapter.Update(view.EmpData);
                UpdateDeparts();
            }
        }

        /// <summary>
        /// Вызывается для обновления списка сотрудников при изменении выбранного отдела
        /// </summary>
        public void UpdateDeparts()
        {
            DataRowView tempDepart = (DataRowView)view.LVDep.SelectedItem;

            view.EmpData.Clear();
            if (tempDepart != null)
            {
                data.EmpAdapter.SelectCommand.Parameters["@Department"].Value = tempDepart.Row["id"];
                data.EmpAdapter.Fill(view.EmpData);
            }
        }

        /// <summary>
        /// Вызывается для удаления выделенного отдела
        /// </summary>
        public void DeleteDepart()
        {
            DataRowView dataRowView = (DataRowView)view.LVDep.SelectedItem;
            dataRowView.Row.Delete();
            data.DepAdapter.Update(view.DepData);
            UpdateDeparts();
        }

        /// <summary>
        /// Вызывается для удаления выделенного сотрудника
        /// </summary>
        public void DeleteEmploye()
        {
            DataRowView dataRowView = (DataRowView)view.DGEmpl.SelectedItem;
            dataRowView?.Row.Delete();
            data.EmpAdapter.Update(view.EmpData);
        }

        /// <summary>
        /// Вызывается для добавления нового отдела
        /// </summary>
        public void AddDepart()
        {
            DataRow dataRow = view.DepData.NewRow();
            AddDepart addDepart = new AddDepart(dataRow);
            addDepart.ShowDialog();
            if (addDepart.DialogResult.HasValue && addDepart.DialogResult.Value)
            {
                view.DepData.Rows.Add(dataRow);
                data.DepAdapter.Update(view.DepData);
            }
        }

        /// <summary>
        /// Вызывается для добавления нового сотрудника
        /// </summary>
        public void AddEmploye()
        {
            if (view.LVDep.SelectedItem != null)
            {
                DataRow dataRow = view.EmpData.NewRow();
                AddEmploy addEmploy = new AddEmploy(dataRow);
                addEmploy.cbEmpDepartmen.ItemsSource = view.DepData.DefaultView;
                addEmploy.cbEmpDepartmen.SelectedItem = (DataRowView)view.LVDep.SelectedItem;
                addEmploy.ShowDialog();
                if (addEmploy.DialogResult.HasValue && addEmploy.DialogResult.Value)
                {
                    view.EmpData.Rows.Add(dataRow);
                    data.EmpAdapter.Update(view.EmpData);
                }
            }
        }
        #endregion
    }
}